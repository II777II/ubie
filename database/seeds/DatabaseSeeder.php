<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);



        $countries = file_get_contents('public/vendor/countries-cities/data.json');

        $countries = json_decode($countries);

        $countries = $countries->countries;

        $cities_to_insert = [];
        $this->command->comment('Started to populate countries and cities');
        foreach ($countries as $country => $cities) {

            $country_id = DB::table('countries')->insertGetId(
                ['name' => $country]
            );

            foreach ($cities as $city) {
                $cities_to_insert[] = ['country_id' => $country_id, 'name' => $city];
            }

            $this->command->info($country_id . " " . $country);

        }

        DB::table('cities')->insert($cities_to_insert);

        $this->command->info('Countries and cities have been populated');

    }
}
