
PALEIDIMAS:
Susikurti duomenų bazę pavadinimu ubie.
Prisijungimo duomenys - vartotojas: admin, slaptažodis: secret.
Pageidaujant kitokių duomenų, pagrindiniame kataloge atsidaryti .env failiuką.

Atsidaryti terminalą.
Pagrindiniame kataloge paleisti komandą:
php artisan migrate
Bus sukurtos duomenų bazės lentelės

Paleisti komandą:
php artisan db:seed
Užpildys šalių ir miestų lenteles duomenimis.

Paleisti komandą:
php artisan serve
Bus paleistas serveris

Naršyklėje suvesti:
localhost:8000
Pasileis aplikacija
