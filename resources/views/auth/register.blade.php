@extends('layouts.app')

@section('content')
<script>
    var countries = <?php echo $countries; ?>;
    var countries = countries.countries;
</script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@if(isset($edit)) Edit @else Register @endif</div>
                <div class="panel-body">
                @if(isset($edit))
                    <form class="form-horizontal" role="form" method="POST" action="/update/{{$user->id}}">
                @else
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                @endif
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{(isset($edit)) ? $user->name : old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                            <label for="surname" class="col-md-4 control-label">Surname</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control" name="surname" value="{{(isset($edit)) ? $user->surname : old('surname') }}" required>

                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            <label for="country" class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                                <select id="country" class="form-control" name="country"></select>

                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-4 control-label">City</label>

                            <div class="col-md-6">
                                <select id="city" class="form-control" name="city"></select>

                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    @if (! isset($edit))
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{(isset($edit)) ? $user->email : old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    @endif
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @if(isset($edit)) Edit @else Register @endif
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    var i = 0;
    var countriesData = [];
    Object.keys(countries).forEach(function (key) {
        countriesData.push({id: i, text: key});
        i++;
    });

    @if (isset($edit))
        var countryValue = '{{ $user->country }}';
        var countryId = getCountryId(countries, countryValue);
    @else
        var countryValue = '';
        var countryId = 0;
    @endif

    var country = Object.keys(countries)[countryId];

    var citiesData = getCitiesDataByCountry(country);

    $('#country').select2({
        data: countriesData
    }).on('select2:select', function(e){
        var country = e.params.data.text;
        var citiesData = getCitiesDataByCountry(country);

        $("#city").select2("destroy");
        $('#city').find('option').remove()
        $('#city').select2({data: citiesData});
    });


    $("#country").select2().select2("val", countryId);


    $('#city').select2({data: citiesData});

    function getCitiesDataByCountry(country) {
        var cities = countries[country];

        var citiesData = [];
        Object.keys(cities).forEach(function (key) {
            citiesData.push({id: key, text: cities[key]});
        });

        return citiesData;
    }

    function getCountryId(countries, countryValue) {
        var i = 0;
        var r = 0;
        Object.keys(countries).forEach(function (key) {
            if (key == countryValue) {
                r = i;
            }
            i++;
        });

        return r;
    }
</script>
@endsection
