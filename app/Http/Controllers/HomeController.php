<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Log;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function edit(User $user) {

        $countries = file_get_contents('vendor/countries-cities/data.json');
        $edit = true;

        return view('auth.register', compact('user', 'countries', 'edit'));
    }



    public function update(Request $request, User $user)
    {

        $data = $request->all();


        $this->validate($request, [
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'password' => 'required|min:8|confirmed',
        ]);

        $country_id = intval($data['country']) + 1;

        $country = DB::table('countries')
            ->where('id', $country_id)
            ->first();
        $city = DB::table('cities')
            ->where('country_id', $country_id)
            ->first();

        $user->update([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'password' => bcrypt($data['password']),
            'country' => $country->name,
            'city' => $city->name]);

        return redirect('/home');
    }

    public function logs() {
        $logs = Log::orderBy('acted', 'desc')->get();

        return view('logs', compact('logs'));
    }

}
